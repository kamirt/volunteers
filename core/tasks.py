import django_rq
from post_office import mail
from django.conf import settings


def mailing(mail_config):
    receivers = mail_config['to']
    from_email = settings.DEFAULT_FROM_EMAIL
    mail.send(
        receivers,
        from_email,
        template='standart_email',
        context={'foo': 'bar'},
        priority='now',
    )

def new_event_notification(event):
    print(event)

def to_archive(event, expire_date):
    print(event, expire_date)


# extern functions

def send_mail_now(mail):
    django_rq.enqueue(mailing, mail_config=mail)

def push_new_event(event):

    users = User.objects.filter()

def send_push(devices, message)

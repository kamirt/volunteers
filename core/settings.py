import os
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

INSTALLED_APPS = [
    'grappelli.dashboard',
    'grappelli',
    'smuggler',
    # 'admin_tools',
    # 'admin_tools.theming',
    # 'admin_tools.menu',
    # 'admin_tools.dashboard',
    'django.contrib.gis',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sites',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'constance.backends.database',
    'constance',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_auth',
    'corsheaders',
    'post_office',
    'easy_thumbnails',
    'django_rq',
    'colorfield',
    'fcm_django',
    #sideapps
    'allauth',
    'allauth.account',
    'rest_auth.registration',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.vk',
    'mapwidgets',
    # apps
    'vprofile',
    'likes',
    'vol_events',
    'notifications'
]

SITE_ID = 1

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'core.middleware.disable_csrf.DisableCSRF'
]

ROOT_URLCONF = 'core.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates'), os.path.join(BASE_DIR, 'templates', 'allauth'), os.path.join(BASE_DIR, 'core', 'templates')],
        # 'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'loaders': [
                'admin_tools.template_loaders.Loader',
                'django.template.loaders.app_directories.Loader',
                'django.template.loaders.filesystem.Loader'
            ]
        },
    },
]

WSGI_APPLICATION = 'core.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators
AUTH_USER_MODEL = 'vprofile.User'

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 6,
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    # {
    #     'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    # },
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(os.path.dirname(__file__), 'static')

MEDIA_ROOT = os.path.join(os.path.dirname(__file__), 'media')

MEDIA_URL = '/media/'

DEFAULT_FROM_EMAIL = 'support@soshelp.online'

MAX_UPLOAD_SIZE = "20971520"
"""
APPLICATIONS
"""

# Rest Framework

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.AllowAny',)
}

# Allauth
UNIQUE_EMAIL = True
TEMPLATE_EXTENSION = 'html'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = True
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 7
ACCOUNT_ADAPTER = 'vprofile.adapter.AccountAdapter'
SOCIALACCOUNT_ADAPTER = 'vprofile.adapter.SocialAccountAdapter'
ACCOUNT_USERNAME_BLACKLIST = ['admin', 'administrator', 'moderator', 'moder']
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
CONFIRM_EMAIL_ON_GET = False
ACCOUNT_LOGOUT_ON_PASSWORD_CHANGE = False
ACCOUNT_LOGOUT_ON_PASSWORD_RESET = False

# Rest auth

LOGOUT_ON_PASSWORD_CHANGE = False
LOGOUT_ON_PASSWORD_RESET = False
REST_AUTH_SERIALIZERS = {
    'LOGIN_SERIALIZER': 'vprofile.serializers.LoginSerializer',
    'PASSWORD_RESET_SERIALIZER': 'vprofile.serializers.PasswordResetSerializer',
    'PASSWORD_RESET_CONFIRM_SERIALIZER': 'vprofile.serializers.PasswordResetConfirmSerializer'
}
REST_AUTH_REGISTER_SERIALIZERS = {
    'REGISTER_SERIALIZER': 'vprofile.serializers.RegisterSerializer'
}
OLD_PASSWORD_FIELD_ENABLED = True
# CORS headers

# Needed to allow cross-origin requests
CORS_ORIGIN_ALLOW_ALL = True

# Constance

CONSTANCE_CONFIG = {
    'EVENT_COUNT_FOR_TRUST':
        (3, 'Количество событий, после проведения которых события пользователя публикуются без модерации', int),
    'DONT_MODERATE_USERS':
        (False, 'Не модерировать вновь зарегистрированных пользователей', bool),
    'MASTER_MAIL':
        ('bio_tech_garden@mail.ru', 'Эл. почта владельца'),
    'DEFAULT_SEARCH_RADIUS':
        (20, 'Радиус поиска по умолчанию, км'),
    'TELEGRAM_RECEIVERS':
        (',', 'ID чатов получателей административных оповещений, через запятую без пробелов'),
}
CONSTANCE_CONFIG_FIELDSETS = {
    'Настройки': ('EVENT_COUNT_FOR_TRUST',
                  'DONT_MODERATE_USERS',
                  'MASTER_MAIL',
                  'DEFAULT_SEARCH_RADIUS',
                  'TELEGRAM_RECEIVERS'),
}
CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'

# Easy thumbnails

THUMBNAIL_BASEDIR = 'thumbs'
THUMBNAIL_EXTENSION = 'jpg'
THUMBNAIL_PRESERVE_EXTENSIONS = ('png', 'jpg', 'jpeg')

THUMBNAIL_ALIASES = {
    '': {
        'avatar_small': {'size': (100, 100), 'crop': True},
        'avatar_usual': {'size': (200, 200), 'crop': True},
        'avatar_big': {'size': (1000, 1000), 'crop': True}
    },
}

# admin-tools

# # ADMIN_TOOLS_THEMING_CSS = 'css/dashboard/theming.css'
# ADMIN_TOOLS_INDEX_DASHBOARD = 'core.dashboard.CustomIndexDashboard'
# ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'core.dashboard.CustomAppIndexDashboard'
# ADMIN_TOOLS_MENU = 'core.menu.CustomMenu'

GRAPPELLI_ADMIN_TITLE = "Волонтеры"
GRAPPELLI_INDEX_DASHBOARD = 'core.dashboard.CustomIndexDashboard'
# map widgets

MAP_WIDGETS = {
    "GOOGLE_MAP_API_KEY": "AIzaSyAvNNmjaXCUydhYkHvch7oeJEMvOG5ffqc",
    "GooglePointFieldWidget": (
        ("zoom", 5),
        ("markerFitZoom", 5),
    )
}

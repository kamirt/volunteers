"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from rest_framework import routers
from django.conf import settings
from django.utils.encoding import iri_to_uri
from django.views.static import serve as staticserve
from .views import doc_view
urlpatterns = [
   re_path(r'^', include('django.contrib.auth.urls')),
   path('doc/', doc_view),
   path('admin/', include('smuggler.urls')),
   path('admin/', admin.site.urls),
   # path('admin_tools/', include('admin_tools.urls')),
   path('grappelli/', include('grappelli.urls')),
   path('api/auth/', include('vprofile.urls')),
   path('api/events/', include('vol_events.urls')),
   path('api/notifications/', include('notifications.urls')),
   path('django-rq/', include('django_rq.urls')),
]

if settings.DEBUG:
    media_url = settings.MEDIA_URL[1:] if settings.MEDIA_URL.startswith('/') \
        else settings.MEDIA_URL

    urlpatterns += (
        re_path(r'^{0}(?P<path>.*)$'.format(iri_to_uri(media_url)),
         staticserve, {'document_root': settings.MEDIA_ROOT}
         ),
    )

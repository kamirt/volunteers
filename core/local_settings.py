DATABASES = {
   'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle', 'django.contrib.gis.db.backends.postgis'
        'NAME': 'volunteers',                             # Or path to database file if using sqlite3.
        'USER': 'volunteer',                             # Not used with sqlite3.
        'PASSWORD': 'vol34',                         # Not used with sqlite3.
        'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',
        'ATOMIC_REQUESTS': True
    }
}

# rq

RQ_QUEUES = {
    'default': {
        'HOST': 'localhost',
        'PORT': 6379,
        'DB': 0,
        'PASSWORD': 'NjksdfHUIfwj293hf2h2ofwJI%o098&ewfh98{}>FH',
        'DEFAULT_TIMEOUT': 360,
    },
}
# RQ_EXCEPTION_HANDLERS = ['path.to.handler']

# telegram

TELEGRAM_TOKEN = '607359944:AAH5DBKxGlLHnF3JHRiWRp6fpmpvL1GDUYQ'
TELEGRAM_CHANNELS = []

FCM_DJANGO_SETTINGS = {
        "FCM_SERVER_KEY": "[your api key]",

        "ONE_DEVICE_PER_USER": False,

        "DELETE_INACTIVE_DEVICES": False,
}

import random
import json
from requests.exceptions import HTTPError
from django.conf import settings
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.auth import get_user_model
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode as uid_decoder
from django.utils.translation import ugettext_lazy as _

from rest_auth.serializers import LoginSerializer as LS
from rest_auth.serializers import PasswordResetSerializer as PRS
from rest_auth.serializers import PasswordResetConfirmSerializer as PRCS
from rest_auth.registration.serializers import RegisterSerializer as RS
from rest_auth.registration.serializers import SocialLoginSerializer
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from allauth.socialaccount.helpers import complete_social_login
from rest_framework.authtoken.models import Token
from .models import User, Organization
# from django.contrib.auth.tokens import default_token_generator


def retrieve_geopoint(coordinates):
    # location = coordinates.split(', ')
    print("CCORDS", coordinates)
    print(coordinates[1])
    print('POINT(%s %s)' % (str(coordinates[1]), str(coordinates[0])))
    location = GEOSGeometry('POINT(%s %s)' % (str(coordinates[1]), str(coordinates[0])), srid=4326)

    return location

class LoginSerializer(LS):
    username = None


class RegisterSerializer(RS):
    username = serializers.CharField(
        max_length=200,
        min_length=1,
        required=True
    )

class VerifyEmailSerializer(serializers.Serializer):
    uid = serializers.IntegerField()
    code = serializers.CharField()

class PasswordResetSerializer(PRS):

    def get_serializer_context(self):
        code = random.randint(111111, 999999)
        self.context['code'] = code
        return {"code":code}

    def get_email_options(self):
        return {
            "extra_email_context": {"code": self.context.get('code')},
            "subject_template_name": "password_reset_subject.txt",
            "email_template_name": "password_reset_email.html",
        }

class PasswordResetConfirmSerializer(PRCS):
    token = None
    code = serializers.IntegerField()

    def validate(self, attrs):
        UserModel = get_user_model()
        self._errors = {}
        try:
            uid = int(attrs['uid'])
            self.user = UserModel._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
            raise ValidationError({'uid': ['Invalid value']})

        self.custom_validation(attrs)
        self.set_password_form = self.set_password_form_class(
            user=self.user, data=attrs
        )
        if not self.set_password_form.is_valid():
            raise serializers.ValidationError(self.set_password_form.errors)

        return attrs

class ProfileSerializer(serializers.ModelSerializer):

    avatar = serializers.ImageField(use_url=True)
    def validate_avatar(self, value):
        print(value)

    def validate_location(self, value):
        try:
            GEOSGeometry(value)
            return value
        except ValueError:
            loc = json.loads(value)
            return retrieve_geopoint(loc)

    def validate_email(self, value):
        if not value:
            raise serializers.ValidationError(_("Поле email не может быть пустым"))
        u = User.objects.filter(email=value)
        if len(u) > 0:
            raise serializers.ValidationError('Пользователь с таким email уже зарегистрирован')

    class Meta:
        model = User
        fields = ('id', 'username', 'email',
                  'avatar', 'phone',
                  'can_create_events', 'blocked',
                  'is_confirmed', 'city', 'location', 'notify_accepted', 'start_notify', 'end_notify')
        read_only_fields = ('can_create_events', 'blocked', 'is_confirmed')

    def get_location(self, obj):
        return str(obj.location)[17:-1].split(' ')

    def get_avatar(self, obj):
        return self.context['request'].build_absolute_uri(obj.avatar.url)

    def get_key(self, obj):
        if self.context['request'].user.pk == obj.pk:
            try:
                t = Token.objects.get(user=obj)
                return t.key
            except:
                return ''
        return ''

class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = ('id', 'name',)

class OrgUserSerializer(serializers.ModelSerializer):
    users = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    class Meta:
        model = Organization
        fields = ('id', 'name', 'users')

class VKOAuth2Serializer(SocialLoginSerializer):
    # email = serializers.CharField(required=False, allow_blank=True)
    user_id = serializers.CharField(required=False, allow_blank=True)

    def validate(self, attrs):
        view = self.context.get('view')
        request = self._get_request()

        if not view:
            raise serializers.ValidationError(_("View is not defined, pass it as a context variable"))

        adapter_class = getattr(view, 'adapter_class', None)
        if not adapter_class:
            raise serializers.ValidationError(_("Define adapter_class in view"))

        adapter = adapter_class(request)
        app = adapter.get_provider().get_app(request)

        # Case 1: We received the access_token
        if attrs.get('access_token'):

            access_data = {
                'access_token': attrs.get('access_token'),
                'user_id': attrs.get('user_id'),
                # 'email': attrs.get('email'),
            }

        # Case 2: We received the authorization code
        elif attrs.get('code'):
            self.callback_url = getattr(view, 'callback_url', None)
            self.client_class = getattr(view, 'client_class', None)

            if not self.callback_url:
                raise serializers.ValidationError(_("Define callback_url in view"))
            if not self.client_class:
                raise serializers.ValidationError(_("Define client_class in view"))

            code = attrs.get('code')

            provider = adapter.get_provider()
            scope = provider.get_scope(request)
            client = self.client_class(
                request,
                app.client_id,
                app.secret,
                adapter.access_token_method,
                adapter.access_token_url,
                self.callback_url,
                scope
            )
            access_data = client.get_access_token(code)

        else:
            raise serializers.ValidationError(_("Incorrect input. access_token or code is required."))

        social_token = adapter.parse_token({'access_token': access_data['access_token']})
        social_token.app = app

        try:
            login = self.get_social_login(adapter, app, social_token, access_data)
            complete_social_login(request, login)
        except HTTPError:
            raise serializers.ValidationError(_('Incorrect value'))

        if not login.is_existing:
            login.lookup()
            login.save(request, connect=True)
        attrs['user'] = login.account.user
        return attrs

class FacebookSerializer(SocialLoginSerializer):
   def validate(self, attrs):
        view = self.context.get('view')
        request = self._get_request()

        if not view:
            raise serializers.ValidationError(
                _("View is not defined, pass it as a context variable")
            )

        adapter_class = getattr(view, 'adapter_class', None)
        if not adapter_class:
            raise serializers.ValidationError(_("Define adapter_class in view"))

        adapter = adapter_class(request)
        app = adapter.get_provider().get_app(request)

        # Case 1: We received the access_token
        if attrs.get('access_token'):
            access_token = attrs.get('access_token')

        # Case 2: We received the authorization code
        elif attrs.get('code'):
            self.callback_url = getattr(view, 'callback_url', None)
            self.client_class = getattr(view, 'client_class', None)

            if not self.callback_url:
                raise serializers.ValidationError(
                    _("Define callback_url in view")
                )
            if not self.client_class:
                raise serializers.ValidationError(
                    _("Define client_class in view")
                )

            code = attrs.get('code')

            provider = adapter.get_provider()
            scope = provider.get_scope(request)
            client = self.client_class(
                request,
                app.client_id,
                app.secret,
                adapter.access_token_method,
                adapter.access_token_url,
                self.callback_url,
                scope
            )
            token = client.get_access_token(code)
            access_token = token['access_token']

        else:
            raise serializers.ValidationError(
                _("Incorrect input. access_token or code is required."))

        social_token = adapter.parse_token({'access_token': access_token})
        social_token.app = app

        try:
            login = self.get_social_login(adapter, app, social_token, access_token)

            complete_social_login(request, login)
        except HTTPError:
            raise serializers.ValidationError(_("Incorrect value"))
        if not login.is_existing:
            # We have an account already signed up in a different flow
            # with the same email address: raise an exception.
            # This needs to be handled in the frontend. We can not just
            # link up the accounts due to security constraints
            # if settings.UNIQUE_EMAIL:
            #     # Do we have an account already with this email address?
            account_exists = get_user_model().objects.filter(
                email=login.user.email,
            )
            print(account_exists)
            if account_exists:
                login.account.user = account_exists[0]
                #     raise serializers.ValidationError(
                #         _("User is already registered with this e-mail address.")
                #     )

            login.lookup()
            login.save(request, connect=True)
        attrs['user'] = login.account.user

        return attrs

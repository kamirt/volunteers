# -*- coding: utf-8 -*-
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class VprofileConfig(AppConfig):
    name = 'vprofile'
    verbose_name = _('Профиль')

    def ready(self):
        import edoprofile.signals

import requests
from django.conf import settings as app_settings
from django.contrib.auth import get_user_model
from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from allauth.account.utils import user_email, user_field, user_username
from allauth.socialaccount.providers.vk.views import VKOAuth2Adapter

from constance import config

class AccountAdapter(DefaultAccountAdapter):
    def send_mail(self, template_prefix, email, context):
        msg = self.render_mail(template_prefix, email, context)
        try:
            msg.send()
        except:
            print("email host does not setted")

    def clean_username(self, username, shallow=False):
        """
        Validates the username. You can hook into this if you want to
        (dynamically) restrict what usernames can be chosen.
        """
        return username

    def save_user(self, request, user, form, commit=True):
        """
        Saves a new `User` instance using information provided in the
        signup form.
        """
        from allauth.account.utils import user_username, user_email, user_field

        data = form.cleaned_data
        first_name = data.get('first_name')
        last_name = data.get('last_name')
        email = data.get('email')
        username = data.get('username')
        user.username = username
        user_email(user, email)
        # user_username(user, username)
        if first_name:
            user_field(user, 'first_name', first_name)
        if last_name:
            user_field(user, 'last_name', last_name)
        if 'password1' in data:
            user.set_password(data["password1"])
        else:
            user.set_unusable_password()
        # если перестали проверять новых пользователей
        user.can_create_events = config.DONT_MODERATE_USERS
        # self.populate_username(request, user)
        if commit:
            # Ability not to commit makes it easier to derive from
            # this adapter by adding
            user.save()
        return user

    def send_confirmation_mail(self, *args, **kwargs):
        print(kwargs)
        # ctx = {
        #     "user": user,
        #     "current_site": site,
        #     "code": code,
        # }
        # if signup:
        #     email_template = 'email_confirmation_signup'
        # else:
        #     email_template = 'email_confirmation'
        # self.send_mail(email_template,
        #                user.email,
        #                ctx)

class SocialAccountAdapter(DefaultSocialAccountAdapter):
    def populate_user(self,
                      request,
                      sociallogin,
                      data):
        username = data.get('username')
        first_name = data.get('first_name')
        last_name = data.get('last_name')
        email = data.get('email')
        name = data.get('name')

        user = sociallogin.user
        # user_username(user, username or data.get('nickname') or data.get('email') or '_')
        # user_email(user, valid_email_or_non-e(email) or '')
        # user_field(user, 'username', '% %' % (first_name, last_name))
        name_parts = (name or '').partition(' ')
        user_field(user, 'first_name', first_name or name_parts[0])
        user_field(user, 'last_name', last_name or name_parts[2])
        user_field(user, 'username', '%s %s' % (first_name, last_name))
        user.can_create_events = config.DONT_MODERATE_USERS
        # user_username(user, '% %' % (first_name, last_name))
        return user

USER_FIELDS = ['first_name',
               'last_name',
               'nickname',
               'screen_name',
               'sex',
               'bdate',
               'city',
               'country',
               'timezone',
               'photo',
               'photo_medium',
               'photo_big',
               'photo_max_orig',
               'has_mobile',
               'contacts',
               'education',
               'online',
               'counters',
               'relation',
               'last_seen',
               'activity',
               'universities']

class VKAdapter(VKOAuth2Adapter):
    def complete_login(self, request, app, token, **kwargs):
        uid = kwargs['response'].get('user_id')
        params = {
            'v': '3.0',
            'access_token': token.token,
            'fields': ','.join(USER_FIELDS),
        }
        if uid:
            params['user_ids'] = uid
        resp = requests.get(self.profile_url,
                            params=params)
        resp.raise_for_status()
        print(resp.json())
        print(self.profile_url)
        print(params)
        try:
            extra_data = resp.json()['response'][0]
            print(resp.json())
            print(self.profile_url)
            print(params)
        except:
            extra_data = {}
        email = kwargs['response'].get('email')
        if email:
            extra_data['email'] = email
        return self.get_provider().sociallogin_from_response(request,
                                                             extra_data)

    def send_confirmation_email(self, *args,  **kwargs):
        print(kwargs)

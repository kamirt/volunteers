from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.contrib.gis.db import models
from mapwidgets.widgets import GooglePointFieldWidget

from django.utils.translation import ugettext_lazy as _
from django import forms

from .models import User, Organization
from .forms import VUserChangeForm, VUserCreationForm



class VUserAdmin(UserAdmin):
    form = VUserChangeForm
    add_form = VUserCreationForm
    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget}
    }
    list_display = ['username', 'email', 'is_staff', 'can_create_events']
    fieldsets = (
        (None, {'fields': ('username', 'email', 'password',)}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'can_create_events')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        (_('Аватар'),
            {'fields': ('avatar',)}),
        (_('Местоположение'),
            {'fields': ('location',)}),
    )

admin.site.register(User, VUserAdmin)

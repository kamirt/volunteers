from rest_auth.registration.views import VerifyEmailView

from django.urls import path, re_path, include
from .views import *

urlpatterns = [
   re_path(r'^password/reset/$', PasswordResetView.as_view(),
       name='rest_password_reset'),
   path('', include('rest_auth.urls')),
   path('registration/', RegistrationView.as_view()),
   path('registration/', include('rest_auth.registration.urls')),
   path('profile/', ProfileView.as_view()),
   path('profile/<int:pk>', ProfileView.as_view()),
   path('organizations/', OrganizationListView.as_view()),
   path('organizations/<int:pk>/', OrganizationDetailView.as_view()),
   path('organizations/<int:pk>/users/', OrganizationUserListView.as_view()),
   re_path(r'^account-confirm-email/', VerifyEmailView.as_view(),
        name='account_email_verification_sent'),
   re_path(r'^account-confirm-email/(?P<key>[-:\w]+)/$', VerifyEmailView.as_view(),
        name='account_confirm_email'),
   # social
   path('social/facebook/', FacebookLoginView.as_view(), name='fb_login'),
   path('social/vk/', VKLoginView.as_view(), name='vk_login'),
   path('social/signup', VKLoginView.as_view(), name='socialaccount_signup'),

   # re_path(r'^password/reset/confirm/$', PasswordResetConfirmView.as_view(),
   #      name='rest_password_reset_confirm'),
]

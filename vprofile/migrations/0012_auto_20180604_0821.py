# Generated by Django 2.0.2 on 2018-06-04 08:21

import django.contrib.gis.db.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vprofile', '0011_auto_20180525_1356'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='location',
            field=django.contrib.gis.db.models.fields.PointField(blank=True, null=True, srid=4326, verbose_name='Геопозиция'),
        ),
    ]

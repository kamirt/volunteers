# Generated by Django 2.0.2 on 2018-06-10 13:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vprofile', '0013_auto_20180608_1319'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='end_notify',
            field=models.TimeField(blank=True, null=True, verbose_name='Время конца уведомлений'),
        ),
        migrations.AddField(
            model_name='user',
            name='notify_accepted',
            field=models.BooleanField(default=True, verbose_name='Уведомления разрешены'),
        ),
        migrations.AddField(
            model_name='user',
            name='start_nofity',
            field=models.TimeField(blank=True, null=True, verbose_name='Время начала уведомлений'),
        ),
    ]

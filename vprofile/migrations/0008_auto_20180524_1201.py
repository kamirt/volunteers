# Generated by Django 2.0.2 on 2018-05-24 12:01

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vprofile', '0007_user_email_verify_code'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmailCode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email_verify_code', models.PositiveIntegerField(blank=True, null=True, verbose_name='Код верификации по email')),
            ],
            options={
                'verbose_name': 'Код верификации',
                'verbose_name_plural': 'Коды верификации',
            },
        ),
        migrations.RemoveField(
            model_name='user',
            name='email_verify_code',
        ),
        migrations.AddField(
            model_name='emailcode',
            name='user',
            field=models.ForeignKey(on_delete='SET_NULL', to=settings.AUTH_USER_MODEL, verbose_name='User'),
        ),
    ]

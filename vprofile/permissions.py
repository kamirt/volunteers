from rest_framework import permissions
from django.utils.translation import ugettext_lazy as _

class BlockedPermission(permissions.BasePermission):
    message = _('Невозможно совершить действие, так как вас добавили в черный список')

    def has_permission(self, request, view):
        return not request.user.blocked

class ModeratedPermission(permissions.BasePermission):
    message = _('Невозможно совершить действие, ваш аккаунт еще не прошел модерацию')

    def has_permission(self, request, view):
        return request.user.can_create_events

import random
import requests
from django.shortcuts import render
from django.contrib.sites.shortcuts import get_current_site
from django.utils.translation import ugettext_lazy as _
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.permissions import (AllowAny,
                                        IsAuthenticated)
from rest_framework.parsers import MultiPartParser, JSONParser, FormParser
from rest_framework import status
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token

from allauth.account.views import ConfirmEmailView
from rest_auth.registration.views import RegisterView
from rest_auth.views import PasswordResetView as PRV
from allauth.account.adapter import get_adapter
from allauth.account.models import EmailAddress
from .serializers import (VerifyEmailSerializer,
                            PasswordResetSerializer,
                            ProfileSerializer,
                            OrganizationSerializer,
                            OrgUserSerializer,
                            VKOAuth2Serializer,
                            FacebookSerializer)
from .adapter import AccountAdapter, VKAdapter
from .models import User, Organization
from .permissions import BlockedPermission, ModeratedPermission
from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from allauth.account.utils import user_email, user_field, user_username
from rest_auth.registration.views import SocialLoginView

import telegram

class VerifyMessage(object):
    def __init__(self, uid, code):
        self.code = code
        self.uid = uid

class RegistrationView(RegisterView):
    def perform_create(self, serializer):
        user = serializer.save(self.request)
        return user

    def get_response_data(self, user):
        email_verify_code = random.randint(111111, 999999)
        verify_message = VerifyMessage(uid=user.pk, code = email_verify_code)
        site = get_current_site(self.request)
        get_adapter(self.request).send_confirmation_mail(user, site, email_verify_code, signup = True)
        serializer = VerifyEmailSerializer(verify_message, context={'request': self.request})
        return serializer.data

class VerifyEmailView(APIView, ConfirmEmailView):
    permission_classes = (AllowAny,)
    allowed_methods = ('POST', 'OPTIONS', 'HEAD')

    def get(self, *args, **kwargs):
        serializer = VerifyEmailSerializer(context={'request': self.request})
        return Response(serializer.data)

    def get_serializer(self, *args, **kwargs):
        return VerifyEmailSerializer(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = User.objects.get(pk=request.data.get('uid'))
            user.is_confirmed = True
            email_user = EmailAddress.objects.get(user=user)
            email_user.verified = True
            email_user.save()
            user.save()
            token, created = Token.objects.get_or_create(user=user)
            channel = settings.TELEGRAM_CHANNELS
            bot = telegram.Bot(settings.TELEGRAM_TOKEN)
            bot.send_message(id_channel, message % (name, question))
        return Response(
            {
                'token': token.key,
                'username': user.username,
                'email': user.email,
                'avatar': user.avatar.url
            }, status=status.HTTP_200_OK)

class PasswordResetView(PRV):
    """
    Сбрасывает пароль, для подтверждения нужно ввести код из почты.
    Также, как и с регистрацией, код возвращается сразу, проверяется на клиенте.
    """
    serializer_class = PasswordResetSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        # Create a serializer with request.data

        serializer = self.get_serializer(data=request.data)
        code = serializer.get_serializer_context().get('code')
        serializer.is_valid(raise_exception=True)
        try:
            serializer.save()
        except:
            print('email does not setted')
        # Return the success message with OK HTTP status
        return Response(
            {"detail": _("Password reset e-mail has been sent."), "code": code},
            status=status.HTTP_200_OK
        )

class ProfileView(generics.RetrieveUpdateAPIView):
    parser_classes = (MultiPartParser, JSONParser, FormParser)
    permission_classes = (IsAuthenticated,)
    serializer_class = ProfileSerializer
    queryset = User.objects.all()

    def retrieve(self, request, pk=None, *args, **kwargs):
        if not pk:
            user = request.user
            serializer = ProfileSerializer(user, context = {'request': request})
            return Response(serializer.data)
        else:
            return super(ProfileView, self).retrieve(request, pk)

    def update(self, request, pk=None, *args, **kwargs):
        if not pk:
            user = request.user
            serializer = ProfileSerializer(user, data=request.data, context = {'request': request}, partial=True)
            print(request.data)
            if serializer.is_valid(raise_exception=True):
                if request.data.get('avatar'):
                    serializer.save(avatar = request.data.get('avatar'))
                else:
                    serializer.save(**serializer.validated_data)
                return Response({"detail": "Данные успешно обновлены"}, status=status.HTTP_200_OK)
            else:
                serializer.save(**serializer.validated_data)
        return super(ProfileView, self).update(request, pk)

class OrganizationListView(generics.ListAPIView):
    """
    Возвращает список всех организаций
    """
    permission_classes = (IsAuthenticated,)
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer

class OrganizationDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
    Добавление, удаление, апдейт организаций
    """
    permission_classes = (IsAuthenticated, BlockedPermission, ModeratedPermission)
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer

class OrganizationUserListView(APIView):
    """
    Возвращает список пользователей организации. Пока без пагинации и только id
    POST - {"uid":<id пользователя>} - добавляет пользователя
    DELETE - {"uid":<id пользователя>} - удаляет пользователя
    """
    permission_classes = (IsAuthenticated, BlockedPermission, ModeratedPermission)
    # serializer_class = OrgUserSerializer
    def get_queryset(self, *args, **kwargs):
        return Organization.objects.all()

    def get_object(self, model, pk):
        try:
            return model.objects.get(pk=pk)
        except model.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        org = self.get_object(Organization, pk)
        serializer = OrgUserSerializer(org)
        return Response(serializer.data)

    def post(self, request, pk, format=None):
        org = self.get_object(Organization, pk)
        user = self.get_object(User, self.request.data.get('uid'))
        org.users.add(user)

        return Response(
            {"detail": _("Пользователь успешно добавлен в организацию")},
            status=status.HTTP_200_OK
        )

    def delete(self, request, pk, format=None):
        org = self.get_object(Organization, pk)
        user = self.get_object(User, self.request.data.get('uid'))
        org.users.delete(user)
        return Response(
            {"detail": _("Пользователь успешно удален из организации")},
            status=status.HTTP_200_OK
        )

class FacebookLoginView(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter
    serializer_class= FacebookSerializer
    permission_classes = (AllowAny,)

class VKLoginView(SocialLoginView):
    adapter_class = VKAdapter
    serializer_class = VKOAuth2Serializer
    permission_classes = (AllowAny,)

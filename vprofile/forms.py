from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from .models import User

class VUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class VUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data['username']
        return username

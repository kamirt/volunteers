# -*- coding: utf-8 -*-
import os
import datetime
from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.db.models import signals as signals
from django.dispatch.dispatcher import receiver
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.conf import settings

from easy_thumbnails.fields import ThumbnailerImageField

class UserManager(BaseUserManager):

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)




class User(AbstractUser):
    username = models.CharField(_('username'), default='', max_length=200)
    avatar = ThumbnailerImageField(
        upload_to='avatars',
        resize_source = dict(size=(200, 200)),
        blank=True,
        default='avatars/default.png')
    email = models.EmailField(_('email address'), unique=False, default="", blank=True, null=True)
    location = models.PointField(_('Геопозиция'), srid=4326, blank=True, null=True)
    birth_date = models.DateField(_('Дата рождения'), null=True, blank=True)
    ip_address = models.GenericIPAddressField(_('IP адрес'), blank=True, null=True)
    phone = models.CharField(_('Телефон'), blank=True, max_length=11, default="")
    address = models.CharField(_('Адрес'), blank=True, max_length=400, default="")
    city = models.CharField(_('Город'), blank=True, max_length=300, default="")
    can_create_events = models.BooleanField(_('Может создавать события'), default=False)
    blocked = models.BooleanField(_('В черном списке'), default=False)
    is_confirmed = models.BooleanField(_('Email подтвержден'), default=False)
    notify_accepted = models.BooleanField(_('Уведомления разрешены'), default=True, blank=True)
    start_notify = models.TimeField(_('Время начала уведомлений'), default=datetime.time(hour=0))
    end_notify = models.TimeField(_('Время конца уведомлений'), default=datetime.time(hour=23, minute=59, second=59))
    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['password', 'username']

    def __str__(self):
        if self.username:
            return self.username
        return self.email

class Organization(models.Model):
    name = models.CharField(_('Название'), max_length=300)
    users = models.ManyToManyField(User, verbose_name=_('Пользователи'), blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Организация'
        verbose_name_plural = 'Организации'

def create_admin_user(app_config, **kwargs):
    if app_config.name != 'vprofile':
        return None
    try:
        User.objects.get(email='admin@localhost')
    except User.DoesNotExist:
        from django.contrib.auth import get_user_model
        print('Creating admin user: email: admin@localhost, password: 123')
        assert get_user_model().objects.create_superuser('admin@localhost', '123')
    else:
        print('Admin user already exists')

signals.post_migrate.connect(create_admin_user)

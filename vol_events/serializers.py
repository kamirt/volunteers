from django.contrib.gis.geos import GEOSGeometry

from rest_framework import serializers

from likes import services as likes_services
from vprofile.serializers import ProfileSerializer
from .models import Event, Contact, Category

def retrieve_geopoint(coordinates):
    # location = coordinates.split(', ')
    location = GEOSGeometry('POINT(%s %s)' % (coordinates[1], coordinates[0]))
    return location

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'color', 'icon')

class ShortCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id',)

class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = ('id', 'name', 'email', 'phone', 'event', 'base')

class EventSerializer(serializers.ModelSerializer):
    is_like = serializers.SerializerMethodField()
    user = ProfileSerializer(read_only=True)
    contacts = serializers.SerializerMethodField()
    location = serializers.CharField()
    distance = serializers.SerializerMethodField()
    category = serializers.SerializerMethodField()

    def validate_category(self, value):
        return value.get('id')

    def validate_location(self, value):
        try:
            GEOSGeometry(value)
            return value
        except ValueError:
            return retrieve_geopoint(value)

    class Meta:
        model = Event
        fields = (
            'id',
            'date_created',
            'likes',
            'is_like',
            'date_begin',
            'date_end',
            'description',
            'user',
            'published',
            'is_verified',
            'category',
            'location',
            'city',
            'address',
            'in_archive',
            'contacts',
            'distance'
        )

    # def get_location(self, obj):
    #     return str(obj.location)[17:-1].split(' ')
    def get_category(self, obj):
        category = {
            "id": obj.category.id,
            "name": obj.category.name,
            "icon": self.context['request'].build_absolute_uri(obj.category.icon.url),
            "color": obj.category.color
        }
        return category

    def get_is_like(self, obj):
        """Проверяет, лайкнул ли `request.user` event (`obj`).
        """
        user = self.context.get('request').user
        return likes_services.is_like(obj, user)

    def get_contacts(self, obj):
        queryset = Contact.objects.filter(event = obj)
        print(queryset)
        if len(queryset) > 0:
            serializer = ContactSerializer(queryset, context={'request': self.context.get('request')}, many=True)
            return serializer.data
        return []

    def get_distance(self, obj):
        if not getattr(obj, 'distance', None):
            return None
        if obj.distance.km < 1:
            return {
                "measure": "m",
                "value": round(obj.distance.m)
            }
        else:
            return {
                "measure": "km",
                "value": round(obj.distance.km, 1)
            }

class ShortEventSerializer(EventSerializer):
    contacts = None
    short_description = serializers.SerializerMethodField()
    category = CategorySerializer()
    class Meta:
        model = Event
        exclude = (
            'date_created',
            'in_archive',
            'date_end',
            'description'
        )
    def get_short_description(self, obj):
        return obj.get_short_description()

# Generated by Django 2.0.2 on 2018-05-29 12:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('vol_events', '0002_auto_20180529_1204'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='vol_events.Category', verbose_name='Категория'),
        ),
    ]

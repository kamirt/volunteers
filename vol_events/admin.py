from django.contrib import admin
from django.contrib.gis.db import models
from .models import Event, Category, Contact
from mapwidgets.widgets import GooglePointFieldWidget
# Register your models here.
class EventAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    change_list_filter_template = "admin/filter_listing.html"
    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget}
    }
    search_fields = ['description', 'city', 'address']
    list_display = ['get_short_description', 'in_archive', 'is_verified', 'city', 'likes']
admin.site.register(Event, EventAdmin)

class CategoryAdmin(admin.ModelAdmin):
    change_list_template = "admin/change_list_filter_sidebar.html"
    change_list_filter_template = "admin/filter_listing.html"


admin.site.register(Category, CategoryAdmin)

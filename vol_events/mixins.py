from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes

from . import services
from .models import Event
from .serializers import EventSerializer

from vprofile.serializers import ProfileSerializer

class EventMixin():
    permission_classes = (IsAuthenticated, )

    @list_route(methods=['GET'])
    def user_events(self, request):
        """Лайкает `obj`.
        """
        user = request.user
        if not user.is_authenticated:
            return Response({"detail": "Учетные данные не были предоставлены."})
        queryset = Event.objects.filter(user=user)
        serializer = EventSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)

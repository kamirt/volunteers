import datetime
import json

from django.contrib.auth import get_user_model
from django.conf import settings
from django.contrib.gis.measure import D
from django.urls import reverse

from constance import config
from fcm_django.models import FCMDevice
from post_office import mail

import django_rq
import telegram
import geocoder



User = get_user_model()

def mailing(*args, **kwargs):
    try:
        receivers = kwargs['to']
        from_email = settings.DEFAULT_FROM_EMAIL
        mail.send(
            receivers,
            from_email,
            template='standart_email',
            context=kwargs.get('context', {}),
            priority='now',
        )
    except:
        pass


def get_address(event):
    coords = str(event.location)[17:-1].split(' ')
    coords = '%s, %s' % (str(coords[0]), str(coords[1]))
    try:
        g = geocoder.yandex(coords, lang='ru-Ru')
        event.city = g.city
        event.address = g.address
        event.save()
    except:
        print(event, "cannot set address!")

def notify(event):
    url = reverse('admin:vol_events_event_change', args=(event.id,))
    message = "Опубликовано новое событие {ename}, от пользователя {user}, ссылка: {url}".format(
        ename=event.get_short_description(),
        user=event.user.username,
        url=url)
    try:
        bot = telegram.Bot(settings.TELEGRAM_TOKEN)
        for receiver in int(config.TELEGRAM_RECEIVERS.split(',')):
            bot.send_message(receiver, message)
    except:
        pass
    mail_config = {
        "to": config.MASTER_MAIL,
        "context": {
            "message": message
        }
    }
    mailing(**mail_config)

def put_in_archive(event):
    event.in_archive = True
    event.is_verified = True
    event.published = False
    event.save()

def send_push(devices, message):
    try:
        django_rq.enqueue(devices.send_message, **message)
    except Exception as e:
        print(e)

def proceed_event(event):
    message = "пользователь {user} опубликовал событие {ename}".format(user=event.user.username,
                                                                       ename=event.get_short_description())
    now = datetime.datetime.now().time()

    users = User.objects.filter(location__distance_lte=(event.location, D(km=config.DEFAULT_SEARCH_RADIUS)),
                                notify_accepted=True,
                                start_notify__lte=now,
                                end_notify__gte=now)

    devices = FCMDevice.objects.filter(user__in=users)
    print(devices)
    message_params = {"title":"Рядом с вами появилось новое событие!",
                      "body":message,
                      "data":{
                             "event": event.pk,
                             "category": event.category.name,
                             }
                      }
    print('sending_push')
    try:
        send_push(devices, message_params)
    except Exception as e:
        print(e)

def notify_master(event):
    django_rq.enqueue(notify, event=event)

def get_event_address(event):
    django_rq.enqueue(get_address, event=event)

def set_expired(event):
    expire_date = event.date_end or event.date_begin
    scheduler = django_rq.get_scheduler('default')
    job = scheduler.enqueue_at(expire_date, put_in_archive, event=event)

from django.apps import AppConfig


class VolEventsConfig(AppConfig):
    name = 'vol_events'

from rest_framework.routers import DefaultRouter

from .views import EventViewSet, CategoryView, EventSearchView
from django.urls import path

router = DefaultRouter()
router.register('', EventViewSet)


urlpatterns = [
    path('categories/', CategoryView.as_view()),
    path('search/', EventSearchView.as_view())
]
urlpatterns += router.urls

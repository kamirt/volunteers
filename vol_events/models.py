from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from easy_thumbnails.fields import ThumbnailerImageField
from colorfield.fields import ColorField
from .tasks import (proceed_event,
                    notify_master,
                    get_event_address,
                    set_expired)
# Create your models here.
class Category(models.Model):
    name = models.CharField(_('Название'), max_length=300)
    icon = ThumbnailerImageField(_('Иконка'),
        upload_to='icons/categories',
        resize_source = dict(size=(200, 200)),
        blank=True)
    color = ColorField(_('Цвет'), default='#FF0000')

    def __str__(self):
        return self.name
    class Meta():
        verbose_name = _("Категория")
        verbose_name_plural = _("Категории")

class Event(models.Model):
    date_created = models.DateTimeField(_('Время создания'), auto_now=True)
    date_begin = models.DateTimeField(_('Время начала'))
    date_end = models.DateTimeField(_('Время окончания'))
    description = models.TextField(_('Описание'))
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('Кто создал'), on_delete=models.CASCADE)
    likes = models.PositiveIntegerField(_('Количество лайков'), default=0, null=True, blank=True)
    published = models.BooleanField(_('Опубликовано'), default=False)
    is_verified = models.BooleanField(_('Проверено'), default=False)
    category = models.ForeignKey(Category, null=True, blank=True, verbose_name=_('Категория'), on_delete=models.SET_NULL)
    location = models.PointField(_('Геопозиция'), srid=4326, blank=True, null=True)
    city = models.CharField(_('Город'), blank=True, max_length=200)
    address = models.CharField(_('Адрес'), blank=True, max_length=500)
    in_archive = models.BooleanField(_('В архиве'), default=False)

    def get_short_description(self):
        if len(self.description) > 100:
            return self.description[:100] + '...'
        return self.description

    def __str__(self):
        return self.get_short_description()

    def save(self, *args, **kwargs):
        if getattr(self, 'is_verified') ==True and getattr(self, 'published') == False and getattr(self, 'in_archive') == False:
            print('SAVING WITH PROCEED...')
            self.published = True
            # try:
            #     proceed_event(self)
            # except:
            #     pass
            super(Event, self).save(*args, **kwargs)

        elif getattr(self, 'is_verified') == False and not self.city:
            # get_event_address(self)
            # notify_master(self)
            # set_expired(self)
            super(Event, self).save(*args, **kwargs)
        else:
            super(Event, self).save(*args, **kwargs)
    class Meta():
        verbose_name = _("Событие")
        verbose_name_plural = _('События')

class Contact(models.Model):
    name = models.CharField(_('Имя'), max_length=200)
    email = models.CharField(_('E-mail'), max_length=200, blank=True)
    phone = models.CharField(_('Телефон'), max_length=15, blank=True)
    event = models.ForeignKey(Event,
                              on_delete=models.SET_NULL,
                              null=True,
                              verbose_name=_('Событие'))
    base = models.BooleanField(_('Основной'), default=False)

    class Meta():
        verbose_name = _("Контакт")
        verbose_name_plural = _('Контакты')

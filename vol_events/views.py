# Create your views here.
import json
from django.contrib.gis.geos import Polygon
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.measure import D
from django.conf import settings
from django.db.models import Q
from rest_framework import viewsets
from rest_framework import generics
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from constance import config

from likes.mixins import LikedMixin

from .models import Event, Contact, Category
from .serializers import (EventSerializer,
                          ShortEventSerializer,
                          CategorySerializer,
                          retrieve_geopoint)
from vprofile.permissions import BlockedPermission, ModeratedPermission
from .mixins import EventMixin
from vprofile import permissions

class CategoryView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

class EventViewSet(LikedMixin, EventMixin, viewsets.ModelViewSet):
    """
    GET, PUT, DELETE api/events/int:id
    Возвращает, обновляет, удаляет конкретное событие.
    Если передать location: api/events/10/?location=[45.00623604595283,%2038.956969847387654]
    то вернется расстояние до события

    GET /api/events/user_events/
    Возвращает список событий пользователя. Пользователь берется из авторизационного заголовка

    GET /api/events/categories/
    Возвращает read_only список категорий

    GET /api/events/int:id/likers/
    Детализованный список лайкнувших событие пользователей

    POST api/events/
    Создает событие.

    POST api/events/int:id/like
    Лайкает событие юзером из заголовка авторизации

    POST api/events/int:id/unlike
    Убирает лайк с события юзером из заголовка авторизации

    """
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        accepted_actions = ['list', 'retrieve', 'detail']
        if self.action in accepted_actions:
            permission_classes = [IsAuthenticatedOrReadOnly]
        else:
            permission_classes = [IsAuthenticated, BlockedPermission, ModeratedPermission]
        return [permission() for permission in permission_classes]

    def create(self, request):
        location = retrieve_geopoint(request.data.get('location').split(','))
        serializer = EventSerializer(data=request.data, context={'request':request})
        category = request.data.get('category').get("id")
        category = Category.objects.get(pk=category)
        if serializer.is_valid():
            """
            Если количество событий в архиве пользователя
            больше conf.EVENT_COUNT_FOR_TRUST, то событие автоматически is_confirmed
            """
            user_event_count = Event.objects.filter(in_archive=True, user=request.user).count()
            trusted = user_event_count > config.EVENT_COUNT_FOR_TRUST
            event = serializer.save(location=location, category=category, user=request.user, is_verified=trusted)
            contacts = request.data.get('contacts')
            if contacts:
                for contact in contacts:
                    Contact.objects.create(
                        name=contact['name'],
                        email=contact['email'],
                        phone=contact['phone'],
                        base=contact['base'],
                        event=event)
        else:
            return Response({"detail": "Неверно заполнены поля", "info": serializer.errors},
                            status.HTTP_400_BAD_REQUEST)
        return Response(serializer.data)

    def list(self, request):
        # queryset = Event.objects.filter(published=True, is_verified=True, in_archive=False)
        # serializer = EventSerializer(queryset, context={'request': request}, many=True)
        return Response([])

    def retrieve(self, request, pk=None, *args, **kwargs):
        location = request.query_params.get('location')
        if location:
            location = json.loads(location)
            location = GEOSGeometry('POINT(%s %s)' % (location[1], location[0]), srid=4326)
            event = Event.objects.annotate(distance=Distance('location', location)).get(pk=pk)
        else:
            event = Event.objects.get(pk=pk)
        serializer = EventSerializer(event, context={"request": request})
        super(EventViewSet, self).retrieve(request, pk)
        return Response(serializer.data)

    def update(self, request, pk=None):
        if self.get_object().in_archive == True:
            return Response({"detail": "Невозможно обновить событие в архиве"},
                            status.HTTP_400_BAD_REQUEST)
        instance = Event.objects.get(pk=pk)
        data = request.data
        exceptions = ["category", "distance", "contacts", "user", "is_like", "location"]
        for key in request.data:
            if not (key in exceptions) and getattr(instance, key):
                setattr(instance, key, data[key])

        categ = request.data.get('category')
        if categ:
            cat_id = categ['id']
            cat = Category.objects.get(pk=cat_id)
            if instance.category != cat.id:
                instance.category = cat
        if data.get('location'):
            location = retrieve_geopoint(request.data.get('location').split(','))
            instance.location = location
        instance.save()
        event = instance
        contacts = request.data.get('contacts')
        cont_ids = []
        if contacts:
            for cont in contacts:
                try:
                    cont_id = cont.get('id')
                    contact = Contact.objects.get(pk=cont_id)

                    for k in cont.keys():
                        if k != 'id' and k != 'event':
                            setattr(contact, k, cont[k])
                    cont_ids.append(contact.id)
                    contact.save()
                except Contact.DoesNotExist:
                    contact = Contact.objects.create(name=cont['name'],
                                                     email=cont['email'],
                                                     phone=cont['phone'],
                                                     base=cont['base'],
                                                     event=event)
                    contact.save()
                    cont_ids.append(contact.pk)
            e_contacts = Contact.objects.filter(event=event)
            for cont in e_contacts:
                if not cont.id in cont_ids:
                    cont.delete()

        return Response({"success": True})

    def partial_update(self, request, pk=None):
        return super(EventViewSet, self).partial_update(request, pk)

    def destroy(self, request, pk=None):
        return super(EventViewSet, self).destroy(request, pk)

class EventSearchView(APIView):
    """
    GET без параметров вернет пустой массив

    Параметр <categories> фильтрует по категориям

    GET с параметром <location> вернет результат поиска по радиусу 20км,
        либо из переданного параметра <radius> в км

    Параметр <bounds> ищет события в определенной зоне, обозначенной границами.

    ВНИМАНИЕ! Если передан bounds и location, то поиск производится внутри bounds,
    а location используется для вычисления расстояния

    Пример запроса поиска по категориям с id=1, а также вокруг определенной точки в радиусе 30км:

    /api/events/search/?location=[45.00623604595283,38.956969847387654]&radius=30&categories=[1]


    Пример передачи bounds.

    Они должны быть на 1 больше углов многоугольника, при этом последняя координата повторяет первую.

    Это нужно для замыкания линии. Например, если передается граница экрана(прямоугольник),

    то нужно передать 5 координат в формате ((lng, lat),(lng, lat),(lng, lat),(lng, lat),(lng, lat)) строкой
    Например:

    /api/events/search/?bounds=[[38.6782149434398,45.103377575254086],[39.2275313496898,45.103377575254086],[39.2275313496898,44.81184405291576],[38.6782149434398,44.81184405291576],[38.6782149434398,45.103377575254086]]&categories=[1]



    Сортировка:

    По усолчанию по date_created, можно указать distance, date_begin, date_end, likes, city

    Пример:

    /api/events/search/?location=[45.00623604595283,38.956969847387654]&radius=30&categories=[1]&order=distance
    """
    permission_classes = (IsAuthenticatedOrReadOnly, )
    allowed_methods = ('GET',)
    def get(self, *args, **kwargs):
        if not self.request.query_params:
            return Response([])
        qparams = self.request.query_params
        categories = qparams.get('categories')
        bounds = qparams.get('bounds')
        location = qparams.get('location')
        order = qparams.get('order', 'date_created')

        radius = qparams.get('radius')
        try:
            events = Event.objects.filter(Q(published=True, is_verified=True) | Q(user=self.request.user))
        except:
            events = Event.objects.filter(published=True, is_verified=True)

        if categories:
            categories = json.loads(categories)
            events = events.filter(category__in=categories)

        if bounds:
            bounds = json.loads(bounds)
            geom = Polygon(tuple(((y, x) for x, y in bounds)), srid=4326)

            events = events.filter(location__intersects=geom)
            if location:
                location = json.loads(location)
                location = GEOSGeometry('POINT(%s %s)' % (location[1], location[0]), srid=4326)
                events = events.annotate(distance=Distance('location', location)).order_by(order)
            else:
                events = events.order_by(order)
            serializer = ShortEventSerializer(events,
                                              many=True,
                                              context={'request': self.request})
            return Response(serializer.data)

        if location:
            location = json.loads(location)
            location = GEOSGeometry('POINT(%s %s)' % (location[1], location[0]),srid=4326)
            radius = radius or config.DEFAULT_SEARCH_RADIUS
            desired_radius = {'km': radius}
            events = events.filter(location__distance_lte=(location, D(**desired_radius))).annotate(distance=Distance('location', location)).order_by(order)
            serializer = ShortEventSerializer(events,
                                              many=True,
                                              context={'request': self.request, 'point': location})
            return Response(serializer.data)
        return Response(serializer.data)
